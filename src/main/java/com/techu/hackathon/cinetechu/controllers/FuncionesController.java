package com.techu.hackathon.cinetechu.controllers;


import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.FuncionesModel;
import com.techu.hackathon.cinetechu.models.SalaModel;
import com.techu.hackathon.cinetechu.services.FuncionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/cinetechu/v1")
public class FuncionesController {

    @Autowired
    FuncionesService funcionesService;

    @GetMapping("/funciones")
    public ResponseEntity<List<FuncionesModel>> obtenerFunciones() {
        System.out.println("obtenerFunciones");

        return new ResponseEntity<>(
                this.funcionesService.findAll(), HttpStatus.OK
        );

    }

    @GetMapping("/cines/{idCine}/salas/{idSala}/funciones")
    public ResponseEntity<Object> getFuncionesBySalas(@PathVariable String idCine,  @PathVariable String idSala) {
        System.out.println("getFuncionesBySalas");

        String msgError = this.funcionesService.validateCineAndSala(idCine, idSala);

        return new ResponseEntity<>(
                msgError != null ? msgError : this.funcionesService.findBySala(idSala) ,
                msgError != null ? HttpStatus.NOT_FOUND : HttpStatus.OK

        );
    }


    @PostMapping("/cines/{idCine}/salas/{idSala}/funciones")
    public ResponseEntity<Object> agregarFuncion(@PathVariable String idCine, @PathVariable String idSala ,@RequestBody FuncionesModel funcion) {
        System.out.println("agregarFuncion");

        funcion.setIdCine(idCine);
        funcion.setIdSala(idSala);

        String msgError = this.funcionesService.validateCineAndSala(idCine, idSala );

        if (msgError == null ){

        msgError = this.funcionesService.validaPelicula(funcion.getIdPelicula());
        }

        return new ResponseEntity<>(
                msgError != null ? msgError : this.funcionesService.add(funcion),
                msgError != null ? HttpStatus.NOT_FOUND : HttpStatus.CREATED

        );
    }

    @PutMapping("/cines/{idCine}/salas/{idSala}/funciones/{id}")
    public ResponseEntity<Object> actualizarFuncion(@PathVariable Map<String, String> pathVars, @RequestBody FuncionesModel funcion) {
        System.out.println("actualizarFuncion");

        String idCine = pathVars.get("idCine");
        String idSala = pathVars.get("idSala");
        String id = pathVars.get("id");

        String msgError = this.funcionesService.validateCineAndSala(idCine, idSala);
        if (msgError == null ){

            msgError = this.funcionesService.validaPelicula(funcion.getIdPelicula());
        }

        Optional<FuncionesModel> funcionactualizado;

        if (msgError == null) {
            funcionactualizado = this.funcionesService.findById(id);

            funcion.setIdCine(idCine);
            funcion.setIdSala(idSala);
            funcion.setId(id);

            if (funcionactualizado.isPresent()) {
                System.out.println("Funcion actualizada");
                this.funcionesService.update(funcion);
            } else {
                msgError = "Función no encontrada";
            }
        }

        return new ResponseEntity<>(
                msgError != null ? msgError : funcion,
                msgError != null ? HttpStatus.NOT_FOUND :HttpStatus.OK
        );
    }

    @DeleteMapping("/cines/{idCine}/salas/{idSala}/funciones/{id}")
    public ResponseEntity<String> eliminarFuncion(@PathVariable Map<String, String> pathVars) {

        System.out.println("eliminarFuncion");

        String idCine = pathVars.get("idCine");
        String idSala = pathVars.get("idSala");
        String id = pathVars.get("id");

        String msgError = this.funcionesService.validateCineAndSala(idCine, idSala);


        if (msgError == null && !this.funcionesService.delete(id) ) {
            msgError = "Función no encontrada";
        }

        return new ResponseEntity<>(
                msgError != null ? msgError : "funcion borrada",
                msgError != null ? HttpStatus.NOT_FOUND : HttpStatus.OK

        );
    }

    private BucketModel validateLugares(FuncionesModel funciones) {
        System.out.println("Validando campos ");
        BucketModel bucket = new BucketModel("",HttpStatus.OK,false);

        if( funciones.getIdSala() == null || funciones.getLugares() == 0 || funciones.getId() == null) {
            bucket.setError(true);
            bucket.setResult("Campos obligatorios ");
            bucket.setStatus(HttpStatus.BAD_REQUEST);
        }

        return bucket;
    }
}
