package com.techu.hackathon.cinetechu.controllers;

import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.EntradaModel;
import com.techu.hackathon.cinetechu.services.EntradaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("cinetechu/v1")
public class EntradaController {

    @Autowired
    EntradaService entradaService;

    @ApiOperation(value = "Mostrar listado de entradas.")
    @ApiResponses(value = {
            @ApiResponse(code = 302, message = "Se encontro la entrada", response = List.class),
            @ApiResponse(code = 204, message = "No se encontro contenido", response = EntradaModel.class)}
            )
    @GetMapping("/entradas")
    public ResponseEntity<Object> getEntradas() {

        System.out.println("getProducts");
        BucketModel bucket = this.entradaService.findAll();
        ResponseEntity<Object> response = new ResponseEntity<>(bucket.getResult(),bucket.getStatus());

        return response;
    }

    @ApiOperation(value = "Mostrar una entrada por medio del id.")
    @ApiResponses(value = {
            @ApiResponse(code = 302, message = "Se encontro la entrada", response = EntradaModel.class),
            @ApiResponse(code = 404, message = "No se encontro la entrada", response = String.class)}
            )
    @GetMapping("/entradas/{id}")
    public ResponseEntity<Object> getEntradasById(@PathVariable String id) {

        System.out.println("getEntradasById");
        System.out.println(id);
        BucketModel bucket = this.entradaService.findById(id);
        ResponseEntity<Object> response = new ResponseEntity<>(bucket.getResult(),bucket.getStatus());

        return response;
    }

    @ApiOperation(value = "Agregar una entrada.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Se encontro la entrada", response = EntradaModel.class),
            @ApiResponse(code = 500, message = "No encontro alguna pelicula, cine o sala", response = String.class)}
    )
    @PostMapping("/entradas")
    public ResponseEntity<Object> addEntrada(@RequestBody EntradaModel entrada) {

        System.out.println("addEntrada");
        System.out.println(entrada.getId());

        BucketModel bucket = entradaService.add(entrada);
        ResponseEntity<Object> response = new ResponseEntity<>(bucket.getResult(),bucket.getStatus());
        return response;

    }

    @ApiOperation(value = "Borrar una entrada.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Se borro la entrada. OK", response = String.class),
            @ApiResponse(code = 404, message = "No encontro la entrada a borrar", response = String.class)}
        )
    @DeleteMapping("/entradas/{id}")
    public ResponseEntity<Object> deleteEntrada(@PathVariable String id){

        System.out.println("deleteEntrada");
        System.out.println(id);
        BucketModel bucket = this.entradaService.delete(id);
        ResponseEntity<Object> response = new ResponseEntity<>(bucket.getResult(),bucket.getStatus());
        return response;
    }

    @ApiOperation(value = "Actualizar una entrada.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Se actualizo la entrada", response = EntradaModel.class),
            @ApiResponse(code = 500, message = "No encontro alguna pelicula, cine o sala", response = String.class),
            @ApiResponse(code = 404, message = "No encontro la entrada", response = String.class)}
    )
    @PutMapping("/entradas/{id}")
    public ResponseEntity<Object> updateEntrada(@RequestBody EntradaModel entrada, @PathVariable String id){

        System.out.println("updateEntrada");
        System.out.println(entrada.getId());

        BucketModel bucket = this.entradaService.update(entrada, id);
        ResponseEntity<Object> response = new ResponseEntity<>(bucket.getResult(),bucket.getStatus());
        return response;
    }
}