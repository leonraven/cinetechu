package com.techu.hackathon.cinetechu.controllers;

import com.techu.hackathon.cinetechu.models.ModeloPelicula;
import com.techu.hackathon.cinetechu.services.ServicioPelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cinetechu/v1")

public class ControladorPelicula {


    @Autowired
    ServicioPelicula servicioPelicula;

    @GetMapping("/peliculas")
    public ResponseEntity<List<ModeloPelicula>> getPelicula(){
        System.out.println("getPelicula");
        return new ResponseEntity<>(
                this.servicioPelicula.findAll(), HttpStatus.OK
        );
    }

    @GetMapping("/peliculas/{id}")
    public ResponseEntity<Object> getPeliculaById(@PathVariable String id){
        System.out.println("pelicula por id");

        Optional<ModeloPelicula> result = this.servicioPelicula.findById(id);

        return new ResponseEntity<>(
                result.isPresent()?result.get():"no se ha encontrado la pelicula",
                result.isPresent()?HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/peliculas")
    public ResponseEntity<Object> agregaPelicula(@RequestBody ModeloPelicula pelicula){
        System.out.println("agrega Pelicula");

       List<String> idCine = pelicula.getCines();

       boolean existe;

       for(String idC:idCine){

        existe = this.servicioPelicula.validateCine(idC);

        if(!existe){
            return new ResponseEntity<>("Cine no encontrado: "+idC, HttpStatus.BAD_REQUEST);
        }
         
        
       }
        return new ResponseEntity<>(
                this.servicioPelicula.add(pelicula), HttpStatus.CREATED
        );

    }

    @PutMapping("/peliculas/{id}")
    public ResponseEntity<Object> actualizaPelicula(@RequestBody ModeloPelicula pelicula, @PathVariable String id){
        System.out.println("actualiza Pelicula");

        boolean actualiza = this.servicioPelicula.update(pelicula, id);

        return new ResponseEntity<>(
                actualiza? pelicula: "no se ha encontrado la pelicula",
                actualiza? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/peliculas/{id}")
    public ResponseEntity<String> eliminaPelicula(@PathVariable String id){
        System.out.println("elimina pelicula");

        boolean elimina = this.servicioPelicula.delete(id);

        return new ResponseEntity<>(
                elimina? "Pelicula eliminada" : "No se pudo eliminar",
                elimina? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}