package com.techu.hackathon.cinetechu.controllers;

import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.SalaModel;
import com.techu.hackathon.cinetechu.services.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cinetechu/v1")
public class SalaController {

    @Autowired
    SalaService salaService;

    @GetMapping("/salas")
    public ResponseEntity<List<SalaModel>> getSalas () {
        System.out.println("getSalas");

        return new ResponseEntity<>( this.salaService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/cines/{idCine}/salas")
    public ResponseEntity<Object> getSalasByCine (@PathVariable String idCine) {
        System.out.println("getSalasByCine");

        BucketModel response = this.salaService.validateCine(idCine);

        if (!response.getError()) {
            response.setResult(this.salaService.findByCine(idCine));
        }

        return new ResponseEntity<> (
                response.getResult(),
                response.getStatus()
        );
    }

    @GetMapping("/cines/{idCine}/salas/{id}")
    public ResponseEntity<Object> getSalaByCineAndId (@PathVariable String idCine, @PathVariable String id) {
        System.out.println("getSalaById");

        BucketModel response = this.salaService.validateCine(idCine);

        if (!response.getError()) {
            Optional<SalaModel> result = this.salaService.findByCineAndId(idCine, id);
            if ( result.isPresent() ) {
                response.setResult(result.get());
            } else {
                response.setResult("Sala no encontrada");
                response.setStatus(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<>(
                response.getResult(),
                response.getStatus()
        );
    }

    @PostMapping("/cines/{idCine}/salas")
    public ResponseEntity<Object> createSala (@PathVariable String idCine, @RequestBody SalaModel sala) {
        System.out.println("createSala");

        BucketModel response = this.salaService.validateCine(idCine);

        if (!response.getError()) response = validateBody(sala);

        if (this.salaService.findById(sala.getId()).isPresent()) {
            response.setResult("La sala ya existe");
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setError(true);
        }

        if (!response.getError()){
            sala.setIdCine(idCine);
            response.setResult(this.salaService.add(sala));
            response.setStatus(HttpStatus.CREATED);
        }

        return new ResponseEntity<>(
                response.getResult(),
                response.getStatus()
        );
    }

    @PutMapping("/cines/{idCine}/salas/{id}")
    public ResponseEntity<Object> updateSala (@PathVariable String idCine, @PathVariable String id, @RequestBody SalaModel sala) {
        System.out.println("updateSala");

        sala.setId(id);
        sala.setIdCine(idCine);

        BucketModel response = this.salaService.validateCine(idCine);

        if (!response.getError()) response = validateBody(sala);

        if (!response.getError()) response = this.salaService.updateById(sala);

        return new ResponseEntity<>(
                response.getResult(),
                response.getStatus()
        );
    }

    @DeleteMapping("/cines/{idCine}/salas/{id}")
    public ResponseEntity<Object> deleteSala (@PathVariable String idCine, @PathVariable String id) {
        System.out.println("deleteSala");

        BucketModel response = this.salaService.validateCine(idCine);

        if (!response.getError()) response = this.salaService.deleteById(id);

        return new ResponseEntity<> (
                response.getResult(),
                response.getStatus()
        );
    }

    private BucketModel validateBody(SalaModel sala) {
        System.out.println("Validando campos obligatorios");
        BucketModel result = new BucketModel("",HttpStatus.OK,false);

        if( sala.getNumeroSala() == null || sala.getCapacidad() == 0 || sala.getId() == null) {
            result.setError(true);
            result.setResult("Campos obligatorios no informados");
            result.setStatus(HttpStatus.BAD_REQUEST);
        }

        return result;
    }

}
