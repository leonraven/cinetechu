package com.techu.hackathon.cinetechu.controllers;

import com.techu.hackathon.cinetechu.Enums.EstadosMexico;
import com.techu.hackathon.cinetechu.Enums.Tipos;
import com.techu.hackathon.cinetechu.models.CineModel;
import com.techu.hackathon.cinetechu.services.CineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cinetechu/v1")
public class CineController {
    @Autowired
    CineService cineService;

    @GetMapping("/cines")
    public ResponseEntity<Object> getCines(@RequestParam(value = "tipo", required = false) Tipos tipo,
                                                    @RequestParam(value = "estado", required = false) EstadosMexico estado,
                                                    @RequestParam(value = "nombre", required = false) String nombre){
        System.out.println("getCines");
        return new ResponseEntity<>(
                this.cineService.findAll(tipo, estado, nombre), HttpStatus.OK
        );
    }

    @GetMapping("/cines/{id}")
    public ResponseEntity<Object> getCineById(@PathVariable String id){
        System.out.println("getCineById");

        Optional<CineModel> result = this.cineService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()?result.get():"Cine no encontrado",
                result.isPresent()?HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/cines")
    public ResponseEntity<CineModel> addCines(@RequestBody CineModel cineModel){
        System.out.println("addCines");

        return new ResponseEntity<>(
                this.cineService.add(cineModel), HttpStatus.CREATED

        );

    }

    @PutMapping("/cines/{id}")
    public ResponseEntity<Object> updateCine(@RequestBody CineModel cineModel, @PathVariable String id){
        System.out.println("updateCine");

        boolean cineToUpdate = this.cineService.update(cineModel, id);

        return new ResponseEntity<>(
                cineToUpdate? cineModel: "Cine no encontrado para actualizar",
                cineToUpdate? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/cines/{id}")
    public ResponseEntity<String> deleteCine(@PathVariable String id){
        System.out.println("deleteCine");

        boolean deleteCine = this.cineService.delete(id);

        return new ResponseEntity<>(
                deleteCine? "Cine Borrado" : "Cine No Borrado",
                deleteCine? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
