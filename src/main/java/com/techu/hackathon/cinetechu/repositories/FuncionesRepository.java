package com.techu.hackathon.cinetechu.repositories;

import com.techu.hackathon.cinetechu.models.FuncionesModel;
import com.techu.hackathon.cinetechu.models.SalaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FuncionesRepository  extends MongoRepository<FuncionesModel , String> {

    @Query("{idSala:?0}")
    List<FuncionesModel> findAllBySala(String idSala);

    @Query("{idSala:?0,numFuncion:?1}")
    Optional<FuncionesModel> findByLugares(String idSala, String numFuncion);

}
