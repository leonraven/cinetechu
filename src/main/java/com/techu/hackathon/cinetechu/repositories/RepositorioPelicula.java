package com.techu.hackathon.cinetechu.repositories;

import com.techu.hackathon.cinetechu.models.ModeloPelicula;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface RepositorioPelicula extends MongoRepository<ModeloPelicula, String>{

    @Query("{nombre:?0}")
    Optional<ModeloPelicula> findPeliculaByName(String nombre);

}