package com.techu.hackathon.cinetechu.repositories;

import com.techu.hackathon.cinetechu.Enums.EstadosMexico;
import com.techu.hackathon.cinetechu.Enums.Tipos;
import com.techu.hackathon.cinetechu.models.CineModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CineRepository extends MongoRepository<CineModel, String> {
    @Query("{'nombre' : ?0}")
    Optional<CineModel> findCineByName(String nombre);

    @Query("{'tipo' : ?0}")
    List<CineModel> findCineModelByTipo(Tipos tipo);

    @Query("{'dirección.estado' : ?0}")
    List<CineModel> findCineModelByEstado(EstadosMexico estado);

    @Query("{'tipo' : ?0, 'dirección.estado' : ?1}")
    List<CineModel> findCineModelByTipoAndEstado(Tipos tipo, EstadosMexico estado);

    @Query("{'tipo' : ?0, 'nombre' : ?1}")
    List<CineModel> findCineModelByTipoAndNombre(Tipos tipo, String nombre);

    @Query("{'dirección.estado' : ?0, 'nombre' : ?1}")
    List<CineModel> findCineModelByEstadoAndNombre(EstadosMexico estado, String nombre);

    @Query("{'tipo' : ?0, 'dirección.estado' : ?1, 'nombre' : ?2}")
    List<CineModel> findCineModelByTipoAndEstadoAndNombre(Tipos tipo, EstadosMexico estado, String Nombre);
}
