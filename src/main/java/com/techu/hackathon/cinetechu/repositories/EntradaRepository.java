package com.techu.hackathon.cinetechu.repositories;

import com.techu.hackathon.cinetechu.models.CineModel;
import com.techu.hackathon.cinetechu.models.EntradaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface EntradaRepository extends MongoRepository<EntradaModel, String> {

}
