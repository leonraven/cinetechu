package com.techu.hackathon.cinetechu.repositories;

import com.techu.hackathon.cinetechu.models.SalaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SalaRepository extends MongoRepository<SalaModel, String> {

    @Query("{idCine:?0,numeroSala:?1}")
    Optional<SalaModel> findBySala(String idCine, String numeroSala);

    @Query("{idCine:?0, id:?1}")
    Optional<SalaModel> findByCineAndId (String idCine, String id);

    @Query("{idCine:?0}")
    List<SalaModel> findAllByCine(String idCine);

}
