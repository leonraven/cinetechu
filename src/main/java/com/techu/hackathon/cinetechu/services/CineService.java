package com.techu.hackathon.cinetechu.services;

import com.techu.hackathon.cinetechu.Enums.EstadosMexico;
import com.techu.hackathon.cinetechu.Enums.Tipos;
import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.CineModel;
import com.techu.hackathon.cinetechu.repositories.CineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CineService {
    @Autowired
    CineRepository cineRepository;

    public Object findAll(Tipos tipo, EstadosMexico estado, String nombre) {
        System.out.println("findAll CineService");
        Object result;
        if(tipo!=null && estado!=null&& nombre!=null){
            result = this.cineRepository.findCineModelByTipoAndEstadoAndNombre(tipo, estado, nombre);
        }else if(tipo!=null && estado!=null){
            result = this.cineRepository.findCineModelByTipoAndEstado(tipo, estado);
        }else if(tipo!=null && nombre!=null){
            result = this.cineRepository.findCineModelByTipoAndNombre(tipo, nombre);
        }else if(estado!=null && nombre!=null){
            result = this.cineRepository.findCineModelByEstadoAndNombre(estado, nombre);
        }else if(nombre!=null){
            result = this.cineRepository.findCineByName(nombre);
        }else if(tipo!=null){
            result = this.cineRepository.findCineModelByTipo(tipo);
        }else if(estado!=null){
            result = this.cineRepository.findCineModelByEstado(estado);
        }
        else{
            result = this.cineRepository.findAll();
        }
        return result;
    }

    public Optional<CineModel> findById(String id) {
        System.out.println("find CineService");
        return this.cineRepository.findById(id);
    }

    public CineModel add(CineModel cineModel){
        System.out.println("add en CineService");
        return this.cineRepository.save(cineModel);
    }

    public boolean update(CineModel cineModel, String id){
        System.out.println("update en CineService");
        boolean result = false;


        if(this.findById(id).isPresent() == true){
            System.out.println("Cine encontrado");
            cineModel.setId(id);
            this.cineRepository.save(cineModel);
            result = true;
        }

        return result;
    }

    public boolean delete(String id){
        System.out.println("delete en CineService");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Cine encontrado y borrando");
            this.cineRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    public BucketModel findCineByName(String nombre) {
        System.out.println("findCineByName");
        Optional<CineModel> optional = this.cineRepository.findCineByName(nombre);
        BucketModel bucket = new BucketModel();
        if(optional.isPresent()){
            bucket.setResult(optional.get());
            bucket.setStatus(HttpStatus.FOUND);
            bucket.setError(false);
        }else{
            bucket.setResult("Cine no encontrado");
            bucket.setStatus(HttpStatus.NOT_FOUND);
            bucket.setError(true);
        }
        return bucket;
    }

}
