package com.techu.hackathon.cinetechu.services;

import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.FuncionesModel;
import com.techu.hackathon.cinetechu.models.ModeloPelicula;
import com.techu.hackathon.cinetechu.repositories.FuncionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class FuncionesService {

    @Autowired
    FuncionesRepository funcionesRepository;

    @Autowired
    CineService cineService;

    @Autowired
    SalaService salaService;

    @Autowired
    ServicioPelicula servicioPelicula;

    public List<FuncionesModel> findAll(){
        System.out.println("findAll");

        return  this.funcionesRepository.findAll();
    }

    public Optional<FuncionesModel>findById(String id){
        System.out.println("Id de la funcion");

        return this.funcionesRepository.findById(id);
    }

    public List<FuncionesModel> findBySala( String idSala){
        System.out.println("findBySala");

        return  this.funcionesRepository.findAllBySala(idSala);
    }

    public FuncionesModel add(FuncionesModel funcion){
        System.out.println("agregar funcion");

        return  this.funcionesRepository.save(funcion);
    }

    public FuncionesModel update(FuncionesModel funciones){
        return this.funcionesRepository.save(funciones);
    }

    public Boolean delete(String id){
        System.out.println("funcion Eliminada");

        boolean result = false;
        if (this.findById(id).isPresent() == true){

            this.funcionesRepository.deleteById(id);
            result = true;
        }
        return  result;

    }

    public String validateCineAndSala(String idCine, String idSala) {
        System.out.println("validateCineAndSala");

        if (this.cineService.findById(idCine).isPresent()) {
           return this.salaService.findByCineAndId(idCine, idSala).isPresent() ? null : "Sala no encontrada";
        }
        else {
            return "Cine no encontrado";
        }
    }
   public String validaPelicula(String idPelicula){
        System.out.println("validapelicula");

        return this.servicioPelicula.findById(idPelicula).isPresent() ? null : "Pelicula no encontrada";

    }

    public BucketModel validaNumlugares(String id , String numlugares){
        System.out.println("validaNumlugares");

        Optional<FuncionesModel> optional = this.funcionesRepository.findByLugares(id,numlugares);
        BucketModel result  = new BucketModel();
        if(optional.isPresent()){
            result.setResult("lugar  no disponible");
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setError(true);
        }else{
            result.setResult(optional.get());
            result.setStatus(HttpStatus.FOUND);
            result.setError(false);
        }
        return result;

    }


}
