package com.techu.hackathon.cinetechu.services;

import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.ModeloPelicula;
import com.techu.hackathon.cinetechu.repositories.RepositorioPelicula;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioPelicula {


    @Autowired
    RepositorioPelicula repositorioPelicula;

    @Autowired
    CineService cineService;


    public List<ModeloPelicula> findAll() {
        System.out.println("find pelicula");

        return this.repositorioPelicula.findAll();
    }

    public ModeloPelicula add(ModeloPelicula pelicula){
        System.out.println("add en servicioPelicula");

        return this.repositorioPelicula.save(pelicula);
    }

    public Optional <ModeloPelicula> findById(String id){
        System.out.println("En ServicioPelicula");

        return this.repositorioPelicula.findById(id);
    }

    public boolean update(ModeloPelicula pelicula, String id){
        System.out.println("actualiza");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            pelicula.setId(id);
            this.repositorioPelicula.save(pelicula);
            result = true;
        }
        return result;

    }

    public boolean delete(String id){
        System.out.println("delete en ServicioPelicula");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("pelicula eliminada");
            this.repositorioPelicula.deleteById(id);
            result = true;
        }

        return result;
    }

    public BucketModel findPeliculaByName(String nombre) {

        System.out.println("findPeliculaByName");
        Optional<ModeloPelicula> optional = this.repositorioPelicula.findPeliculaByName(nombre);
        BucketModel bucket = new BucketModel();

        if(optional.isPresent()){
            bucket.setResult(optional.get());
            bucket.setStatus(HttpStatus.FOUND);
            bucket.setError(false);
        }else{
            bucket.setResult("Pelicula no encontrada");
            bucket.setStatus(HttpStatus.NOT_FOUND);
            bucket.setError(true);
        }
        return bucket;
    }


    public boolean validateCine(String idCine) {
        boolean cine = false;

        if (this.cineService.findById(idCine).isPresent()) {
            cine = true;
        }

        return cine;

    }


}