package com.techu.hackathon.cinetechu.services;

import com.techu.hackathon.cinetechu.models.*;
import com.techu.hackathon.cinetechu.repositories.EntradaRepository;
import com.techu.hackathon.cinetechu.repositories.RepositorioPelicula;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class EntradaService {

    @Autowired
    EntradaRepository entradaRepository;
    @Autowired
    ServicioPelicula servicioPelicula;
    @Autowired
    CineService cineService;
    @Autowired
    SalaService salaService;

    public BucketModel findAll() {
        System.out.println("findAll");
        List<EntradaModel> entradas = this.entradaRepository.findAll();
        BucketModel bucket = new BucketModel();
        if(entradas.size()>0){
            bucket.setResult(entradas);
            bucket.setStatus(HttpStatus.FOUND);
            bucket.setError(false);
        }else {
            bucket.setResult(entradas);
            bucket.setStatus(HttpStatus.NO_CONTENT);
            bucket.setError(false);
        }

        return bucket;
    }

    public BucketModel findById(String id) {
        System.out.println("findById");
        Optional<EntradaModel> entradaOption = this.entradaRepository.findById(id);
        BucketModel bucket = new BucketModel();
        if(entradaOption.isPresent()){
            bucket.setResult(entradaOption.get());
            bucket.setStatus(HttpStatus.FOUND);
            bucket.setError(false);
        }else{
            bucket.setResult("No se encontro la entrada");
            bucket.setStatus(HttpStatus.NOT_FOUND);
            bucket.setError(true);
        }

        return bucket;
    }

    public BucketModel add(EntradaModel entrada) {
        System.out.println("add EntradaService");

        BucketModel bucket = new BucketModel();
        BucketModel bucketCine = this.cineService.findCineByName(entrada.getCine());
        BucketModel bucketPelicula = this.servicioPelicula.findPeliculaByName(entrada.getPelicula());

        if(!bucketCine.getError()){
            if(!bucketPelicula.getError()) {
                CineModel cine = (CineModel) bucketCine.getResult();
                BucketModel bucketSala = this.salaService.findSalaByNumber(cine.getId(),entrada.getSala());
                if(!bucketSala.getError()) {
                    EntradaModel entradaCreada = this.entradaRepository.save(entrada);
                    if (entradaCreada != null) {
                        bucket.setResult(entradaCreada);
                        bucket.setStatus(HttpStatus.CREATED);
                        bucket.setError(false);
                    } else {
                        bucket.setResult("No se pudo crear la entrada");
                        bucket.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        bucket.setError(true);
                    }
                }else{
                    bucket = bucketSala;
                }
            }else{
                bucket = bucketPelicula;
            }
        }else{
            bucket = bucketCine;
        }


        return bucket;
    }

    public BucketModel delete(String id){
        System.out.println("update EntradaService");
        BucketModel bucket = new BucketModel();
        if(!this.findById(id).getError()){
            this.entradaRepository.deleteById(id);
            bucket.setResult("Entrada borrada con exito.");
            bucket.setStatus(HttpStatus.OK);
            bucket.setError(false);
        }else{
            bucket.setResult("No se encontro la entrada a borrar");
            bucket.setStatus(HttpStatus.NOT_FOUND);
            bucket.setError(true);
        }

        return bucket;
    }

    public BucketModel update(EntradaModel entrada, String id){
        System.out.println("update ProductService");
        entrada.setId(id);
        BucketModel bucket = new BucketModel();
        BucketModel bucketCine = this.cineService.findCineByName(entrada.getCine());
        BucketModel bucketPelicula = this.servicioPelicula.findPeliculaByName(entrada.getPelicula());

        if(!this.findById(id).getError()){
            if(!bucketCine.getError()) {
                if (!bucketPelicula.getError()) {
                    CineModel cine = (CineModel) bucketCine.getResult();
                    BucketModel bucketSala = this.salaService.findSalaByNumber(cine.getId(), entrada.getSala());
                    if (!bucketSala.getError()) {
                        bucket.setResult("Se actualizo con exito");
                        bucket.setStatus(HttpStatus.ACCEPTED);
                        bucket.setError(false);
                    }else{
                        bucket.setResult("No se encontro la sala");
                        bucket.setStatus(HttpStatus.BAD_REQUEST);
                        bucket.setError(true);
                    }
                }else{
                    bucket.setResult("No se encontro la pelicula");
                    bucket.setStatus(HttpStatus.BAD_REQUEST);
                    bucket.setError(true);
                }
            }else{
                bucket.setResult("No se encontro el cine");
                bucket.setStatus(HttpStatus.BAD_REQUEST);
                bucket.setError(true);
            }
        }else{
            bucket.setResult("No se encontro la entrada a actualizar");
            bucket.setStatus(HttpStatus.BAD_REQUEST);
            bucket.setError(true);
        }

        return bucket;
    }
}
