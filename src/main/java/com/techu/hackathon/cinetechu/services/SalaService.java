package com.techu.hackathon.cinetechu.services;

import com.techu.hackathon.cinetechu.models.BucketModel;
import com.techu.hackathon.cinetechu.models.CineModel;
import com.techu.hackathon.cinetechu.models.SalaModel;
import com.techu.hackathon.cinetechu.repositories.SalaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SalaService {

    @Autowired
    SalaRepository salaRepository;

    @Autowired
    CineService cineService;

    public List<SalaModel> findByCine(String idCine) {
        System.out.println("findByCine() SalaService");

        return this.salaRepository.findAllByCine(idCine);
    }

    public List<SalaModel> findAll() {
        System.out.println("findAll() SalaService");

        return this.salaRepository.findAll();
    }

    public Optional<SalaModel> findById(String id) {
        System.out.println("findById() SalaService");

        return this.salaRepository.findById(id);
    }

    public Optional<SalaModel> findByCineAndId(String idCine, String id) {
        System.out.println("findById() SalaService");

        return this.salaRepository.findByCineAndId(idCine, id);
    }

    public SalaModel add(SalaModel sala) {
        System.out.println("add() SalaService");

        return this.salaRepository.save(sala);
    }

    public BucketModel updateById(SalaModel sala) {
        System.out.println("updateById() SalaService");
        BucketModel result = new BucketModel("",HttpStatus.OK,false);

        if (!this.findByCineAndId(sala.getIdCine(), sala.getId()).isPresent()) {
            result.setResult("Sala no encontrada");
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setError(true);
        } else {
            result.setResult(this.salaRepository.save(sala));
        }

        return result;
    }

    public BucketModel deleteById(String id) {
        System.out.println("deleteById() SalaService");
        BucketModel result = new BucketModel("",HttpStatus.OK,false);

        if (!this.salaRepository.findById(id).isPresent()) {
            result.setResult("Sala no encontrada");
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setError(true);
        } else {
            this.salaRepository.deleteById(id);
            result.setResult("Sala eliminada");
        }

        return result;
    }

    public BucketModel validateCine(String idCine) {
        System.out.println("validateCine() SalaService");
        BucketModel result = new BucketModel("", HttpStatus.OK,false);

        if(!this.cineService.findById(idCine).isPresent()) {
            result.setError(true);
            result.setResult("Cine no encontrado");
            result.setStatus(HttpStatus.NOT_FOUND);
        }

        return result;
    }

    public BucketModel findSalaByNumber(String id, String numeroSala) {
        System.out.println("findSalaByNumber");
        Optional<SalaModel> optional = this.salaRepository.findBySala(id, numeroSala);
        BucketModel bucket = new BucketModel();
        if(optional.isPresent()){
            bucket.setResult(optional.get());
            bucket.setStatus(HttpStatus.FOUND);
            bucket.setError(false);
        }else{
            bucket.setResult("Sala no encontrada");
            bucket.setStatus(HttpStatus.NOT_FOUND);
            bucket.setError(true);
        }
        return bucket;
    }
}
