package com.techu.hackathon.cinetechu.models;

import com.techu.hackathon.cinetechu.Enums.EstadosMexico;
import org.springframework.data.mongodb.core.index.Indexed;

public class DirecciónModel {
    private String calle;
    private String número;
    private String colonia;
    private String delegación;
    private String cp;
    @Indexed
    private EstadosMexico estado;

    public DirecciónModel() {
    }

    public DirecciónModel(String calle, String número, String colonia, String delegación, String cp, EstadosMexico estado) {
        this.calle = calle;
        this.número = número;
        this.colonia = colonia;
        this.delegación = delegación;
        this.cp = cp;
        this.estado = estado;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNúmero() {
        return número;
    }

    public void setNúmero(String número) {
        this.número = número;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getDelegación() {
        return delegación;
    }

    public void setDelegación(String delegación) {
        this.delegación = delegación;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public EstadosMexico getEstado() {
        return estado;
    }

    public void setEstado(EstadosMexico estado) {
        this.estado = estado;
    }
}
