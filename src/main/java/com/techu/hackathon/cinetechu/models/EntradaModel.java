package com.techu.hackathon.cinetechu.models;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "entradas")
public class EntradaModel {

    @Id
    @ApiModelProperty(notes = "Id de la entrada",name="id",required=true,value="1")
    private String id;
    @ApiModelProperty(notes = "Nombre del cliente",name="nombreCliente",required=true,value="Juan")
    private String nombreCliente;
    @ApiModelProperty(notes = "Nombre del cine",name="cine",required=true,value="Chinepolis")
    private String cine;
    @ApiModelProperty(notes = "Nombre de la pelicula",name="pelicula",required=true,value="Pelicula 2")
    private String pelicula;
    @ApiModelProperty(notes = "Nombre de la sala",name="sala",required=true,value="SALA 1")
    private String sala;
    @ApiModelProperty(notes = "Descripcion del horario",name="horario",required=true,value="9:00 - 12:05")
    private String horario;
    @ApiModelProperty(notes = "Asientos comprados",name="asientos",required=true,value="A1,A2")
    private String asientos;

    public EntradaModel() {
    }

    public EntradaModel(String id, String nombreCliente, String cine, String pelicula, String sala, String horario, String asientos) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.cine = cine;
        this.pelicula = pelicula;
        this.sala = sala;
        this.horario = horario;
        this.asientos = asientos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getCine() {
        return cine;
    }

    public void setCine(String cine) {
        this.cine = cine;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getAsientos() {
        return asientos;
    }

    public void setAsientos(String asientos) {
        this.asientos = asientos;
    }
}
