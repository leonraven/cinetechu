package com.techu.hackathon.cinetechu.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "salas")
public class SalaModel {

    @Id
    private String id;
    private String idCine;
    private String numeroSala;
    private int capacidad;

    public SalaModel() {
    }

    public SalaModel(String id, String idCine, String numeroSala, int capacidad) {
        this.id = id;
        this.idCine = idCine;
        this.numeroSala = numeroSala;
        this.capacidad = capacidad;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCine() {
        return this.idCine;
    }

    public void setIdCine(String idCine) {
        this.idCine = idCine;
    }

    public String getNumeroSala() {
        return this.numeroSala;
    }

    public void setNumeroSala(String numeroSala) {
        this.numeroSala = numeroSala;
    }

    public int getCapacidad() {
        return this.capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }
}
