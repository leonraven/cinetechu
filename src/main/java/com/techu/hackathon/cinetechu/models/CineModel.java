package com.techu.hackathon.cinetechu.models;

import com.techu.hackathon.cinetechu.Enums.Tipos;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "cines")
public class CineModel {
    @Id
    private String id;
    private String nombre;
    @Indexed
    private Tipos tipo;
    private DirecciónModel dirección;

    public CineModel() {
    }

    public CineModel(String id, String nombre, Tipos tipo, DirecciónModel dirección) {
        this.id = id;
        this.nombre = nombre;
        this.tipo = tipo;
        this.dirección = dirección;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Tipos getTipo() {
        return tipo;
    }

    public void setTipo(Tipos tipo) {
        this.tipo = tipo;
    }

    public DirecciónModel getDirección() {
        return dirección;
    }

    public void setDirección(DirecciónModel dirección) {
        this.dirección = dirección;
    }
}
