package com.techu.hackathon.cinetechu.models;

import org.springframework.http.HttpStatus;

public class BucketModel {

    private Object result;
    private HttpStatus status;
    private boolean error;

    public BucketModel(Object result, HttpStatus status, boolean error) {
        this.setResult(result);
        this.setStatus(status);
        this.setError(error);
    }

    public BucketModel() {
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}