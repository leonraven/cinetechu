package com.techu.hackathon.cinetechu.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "peliculas")
public class ModeloPelicula {
    @Id
    private String id;
    private String nombre;
    private String genero;
    private String fechaEstreno;
    private List<String> cines;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(String fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public List<String> getCines() {
        return cines;
    }

    public void setCines(List<String> cines) {
        this.cines = cines;
    }
    
}