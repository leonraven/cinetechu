package com.techu.hackathon.cinetechu.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "funciones")

public class FuncionesModel {

    @Id
    private String id;
    private int lugares;
    private String horario;
    private String idCine;
    private  String idSala;
    private String idPelicula;

    public FuncionesModel() {
    }

    public FuncionesModel(String id, String idCine, String idSala, String idPelicula, int lugares, String horario) {
        this.id = id;
        this.idCine = idCine;
        this.idSala = idSala;
        this.idPelicula = idPelicula;
        this.lugares = lugares;
        this.horario = horario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCine() { return idCine; }

    public void setIdCine(String idCine) { this.idCine = idCine; }

    public String getIdSala() {
        return idSala;
    }

    public void setIdSala(String idSala) {
        this.idSala = idSala;
    }

    public String getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(String idPelicula) {
        this.idPelicula = idPelicula;
    }

    public int getLugares() {
        return lugares;
    }

    public void setLugares(int lugares) {
        this.lugares = lugares;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}

